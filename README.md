# Boolean UI Challenge

Thank you for having a look at my submission! In this document I will give a general overview of the front end, how to setup locally, and future improvements.

## UI Summary

Users can view a summary of profiles and see the posts made by those profiles. Each post shows a summary of the likes and comments, and users can choose to show all comments if they wish.

## Local Setup

Pre-requisites: You must have NodeJs (Version 14 or higher) installed.

To setup the UI locally run the following commands:

```
# Install dependencies
npm i

# Start local dev server
npm start
```

## Improvements / Considerations

1. Would implement search, like and other functionality found in Instagram, using API requests and structuring the components such that event handling logic is abstracted out to parent components to keep child components as simple as possible.

2. Use request / network library that adds further functionality to making API request, such as request caching, re-fetching, parallel requests, cancellation etc.

3. Use sass instead of CSS for better styling structure using CSS variables, nesting, mixins etc.

4. Implement responsiveness and accessibility.
