import { useEffect, useState } from "react";
import FollowedPosts from "./FollowedPosts";
import Profiles from "./Profiles";
import "./Feed.css";

async function fetchProfiles({ setProfiles }) {
  const profilesResponse = await fetch(
    "https://flynn.boolean.careers/exercises/api/boolgram/profiles"
  );
  const profilesJson = await profilesResponse.json();
  setProfiles(profilesJson);
}

async function fetchPosts({ setPosts }) {
  const postsResponse = await fetch(
    "https://flynn.boolean.careers/exercises/api/boolgram/posts"
  );
  const postsJson = await postsResponse.json();
  setPosts(postsJson);
}

function Feed() {
  const [profiles, setProfiles] = useState([]);
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetchProfiles({ setProfiles });
    fetchPosts({ setPosts });
  }, []);

  return (
    <div className="Feed">
      <Profiles profiles={profiles} />
      <FollowedPosts posts={posts} />
    </div>
  );
}

export default Feed;
