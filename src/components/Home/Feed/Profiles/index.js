import Image from "../../../Image";
import "./Profiles.css";

export function Profile({ profile }) {
  return (
    <div className="Profile">
      <Image imageClass="profileImage" src={profile.profile_picture} />
      <p>{profile.profile_name}</p>
    </div>
  );
}

function Profiles({ profiles }) {
  return (
    <div className="Profiles">
      {profiles.map((profile) => (
        <Profile profile={profile} />
      ))}
    </div>
  );
}

export default Profiles;
