import { useState } from "react";
import Image from "../../../Image";
import "./FollowedPosts.css";

function CommentForm({ postId }) {
  const [commentInput, setCommentInput] = useState("");

  const onCommentChange = (e) => {
    setCommentInput(e.target.value);
  };

  const postComment = async () => {
    await fetch(`http://localhost:3000/${postId}/comment`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        comment: commentInput,
      }),
    });
  };

  const onCommentSubmit = (e) => {
    e.preventDefault();
    postComment();
    setCommentInput("");
  };

  return (
    <form name="commentForm" onSubmit={onCommentSubmit}>
      <div className="postComment">
        <input
          name="comment"
          type="text"
          placeholder="Comment"
          onChange={onCommentChange}
          value={commentInput}
        />
        <input className="commentSubmit" type="submit" value="Publish" />
      </div>
    </form>
  );
}

function Post({ post }) {
  const [showAllComments, setShowAllComments] = useState(false);

  const onShowComments = () => {
    setShowAllComments(!showAllComments);
  };

  const createComments = (comments) => {
    return comments.map((comment) => (
      <p>
        <strong>{comment.username}</strong> {comment.text}
      </p>
    ));
  };

  const mostRecentComments = post.comments.slice(-3);

  return (
    <div className="Post">
      <div className="postHeader">
        <Image imageClass="postProfileImage" src={post.profile_picture} />
        <p>{post.profile_fullname}</p>
      </div>
      <Image imageClass="postImage" src={post.post_image} />
      <div className="postContent">
        <div className="postControls">
          <i>Like</i>
          <i>Comment</i>
        </div>
        <div className="postLikes">
          {post.likes.length ? (
            <>
              <Image
                imageClass="postLikeImage"
                src={post.likes[0].profile_picture}
              />
              <p>
                {post.likes[0].username} and {post.likes.length} others have
                liked
              </p>
            </>
          ) : (
            <p>No likes yet</p>
          )}
        </div>
        <p>{post.post_text}</p>
        <div className="postComments">
          <p onClick={onShowComments}>
            {post.comments.length === 3 || post.comments.length === 0
              ? "Comments"
              : `Show all ${post.comments.length} comments`}
          </p>
          {showAllComments
            ? createComments(post.comments)
            : createComments(mostRecentComments)}
        </div>
      </div>
      <CommentForm postId="post-id" />
    </div>
  );
}

function FollowedPosts({ posts }) {
  return (
    <div className="FollowedPosts">
      {posts.map((post) => (
        <Post post={post} />
      ))}
    </div>
  );
}

export default FollowedPosts;
