import { useContext } from "react";
import Feed from "./Feed";
import Header from "../Header";
import "./Home.css";

function Home() {
  return (
    <div className="home">
      <Header logoSrc={"https://google.com/img"} />
      <Feed />
    </div>
  );
}

export default Home;
