function Image({ src, alt, navigateTo, className, imageClass }) {
  const img = <img className={imageClass} src={src} alt={alt ? alt : ""} />;
  return (
    <div className={className}>
      {navigateTo ? <a href={navigateTo}>{img}</a> : img}
    </div>
  );
}

export default Image;
