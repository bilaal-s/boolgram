import Image from "../../Image";
import "./Controls.css";
import logo from "../../../logo.svg";

function Controls({ profileSrc }) {
  return (
    <div className="Controls">
      <i>Like</i>
      <i>Home</i>
      <Image imageClass="controlProfileImage" src={logo} />
    </div>
  );
}

export default Controls;
