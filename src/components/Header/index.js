import Search from "./Search";
import Controls from "./Controls";
import "./Header.css";

function Header({ logoSrc }) {
  return (
    <div className="Header">
      <h1>Boolgram</h1>
      <Search />
      <Controls />
    </div>
  );
}

export default Header;
